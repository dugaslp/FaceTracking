
#include <iostream>
#include <chrono>
#include <thread>

using namespace std;
using namespace chrono;
using namespace this_thread;

void task1() {
	sleep_for(1s);
	cout << "I did not hit her." << endl;
	sleep_for(1s);
	cout << "It's not true." << endl;
	sleep_for(1s);
	cout << "It's bullshit." << endl;
	sleep_for(1s);
	cout << "I did not hit her." << endl;
	sleep_for(1s);
	cout << "I did nooooooooooot." << endl;
}