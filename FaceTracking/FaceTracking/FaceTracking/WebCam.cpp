#include "FaceTracking.hpp"


void WebCam() {

	//Declarations
	namedWindow("Video", WINDOW_AUTOSIZE);	
	VideoCapture cap;	//Video capture declaration
	Mat frame;		//Video frame taken raw from the webcam
	Mat frameOut;	//Webcam video frame after treatment
	

	//Start recording from the webcam in the capture
	cap.open(0);


	//Live streams the webcam input with applied mask, using a loophole with break conditions
	for (;;)
	{
		//For each cycle, the frame shifts to the next one in the capture
		cap >> frame;

		//Breaks the loop when the frame has accessed the 
		if (frame.empty()) { break; }

		//Blurs the frames
		GaussianBlur(frame, frame, Size(5, 5), 3, 3);

		//Converts the colored image (default is BGR) to gray scales
		cvtColor(frame, frameOut, COLOR_BGR2GRAY);

		//Applies gradient filter to highlight edages in the stream frames
		Sobel(frameOut, frameOut, -1, 1, 0, 3);

		//Shows the result in the window
		imshow("Video", frameOut);

		//Wait 30 ms between each frame and stops the stream if a key is pressed
		if (waitKey(30) >= 0) break;
	}


	//Closes all instances at the end of the function  (Does not work well)
	destroyWindow("Video"); 

	return;


}